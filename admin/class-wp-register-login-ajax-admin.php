<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://github.com/royrakesh
 * @since      1.0.0
 *
 * @package    Wp_Register_Login_Ajax
 * @subpackage Wp_Register_Login_Ajax/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Wp_Register_Login_Ajax
 * @subpackage Wp_Register_Login_Ajax/admin
 * @author     Rakesh Roy <rakesh@appycodes.com>
 */
class Wp_Register_Login_Ajax_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wp_Register_Login_Ajax_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wp_Register_Login_Ajax_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wp-register-login-ajax-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wp_Register_Login_Ajax_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wp_Register_Login_Ajax_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/wp-register-login-ajax-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Function add menu to wordpress Menu
	 */


		

		public function wrla_options_menu_link(){

			add_options_page(
			'Wordpress Register Login Ajax',
			'W RLA Menu',
			'manage_options',
			'wrla_options',
			array(
                $this,
                'wrla_options_form'
            )
			);


			 
		}

		public function wrla_options_form(){
			
			echo 'Hello';
		}

		

		public function wrla_register_settings(){
			register_setting( 'wrla_settings_group', 'wrla_settings');
		}
}



























