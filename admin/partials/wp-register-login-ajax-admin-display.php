<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://github.com/royrakesh
 * @since      1.0.0
 *
 * @package    Wp_Register_Login_Ajax
 * @subpackage Wp_Register_Login_Ajax/admin/partials
 */
