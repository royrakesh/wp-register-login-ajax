(function($) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 *
	 *
	 */



	$('#wp_rla_login_button').click(function() {

		let username = $('#wp_rla_login_username').val();
		let password = $('#wp_rla_login_password').val();
		let remember = $('input[name="wp_rla_login_remember"]:checked').val()

		let data = {
			action: 'wrla_login',
			_ajax_nonce: wrlaobj.wprla_nonce,
			wprla_username : username,
			wprla_password:  password,
			wprla_remember : remember
		};
		
		let ajaxurl = wrlaobj.wprla_url;

		jQuery.post(ajaxurl, data, function(response) {

			let objwrla = JSON.parse(response);


			if(objwrla.error == 'true'){
				$( "#wprla_message_error" ).css('display','block');
				$( "#wprla_message_error" ).html(objwrla.msg);
			    }else{

				let msg = objwrla.msg + ' You will be redirected after ';
				$( "#wprla_message_error" ).css('display','none');
				$( "#wprla_message_success" ).css('display','block');
				
				let seconds = 5;
				$( "#wprla_message_success" ).html(msg + seconds + ' seconds');
				
				setInterval(function () {
                    seconds--;
                   $( "#wprla_message_success" ).html(msg + seconds + ' seconds');                  
                    if (seconds < 1) {
                        window.location = wrlaobj.wprla_redirect_url
                    }
                }, 1000);

			    }

		});


	});



})(jQuery);