<?php

function wp_ajax_register_login_login_form() {


?>

<div class="wp-ajax-rl">
  <form action="" id="wp_rla_login">

   <div id="wprla_message_success"></div>
   <div id="wprla_message_error"></div>
   
    <div class="wprla-container">
    <label for="username">Username / Email</label>
    <input type="text" id="wp_rla_login_username" name="wp_rla_login_username" placeholder="Enter your Username" required="">
  
    </div>

    <div class="wprla-container">
    <label for="password">Password</label>
    <input type="password" id="wp_rla_login_password" name="wp_rla_login_password" placeholder="Enter your password">
    </div>
    <div class="wprla-container">

    <input type="checkbox" id="wp_rla_login_remember" name="wp_rla_login_remember" value="1">
        <label for="password">Remember Me</label>
    </div>

    <div class="wprla-button-container">
    <input type="button" value="Login" id="wp_rla_login_button">
    </div>

  </form>
</div>

<?php		

}

add_shortcode( 'wp-ajax-register', 'wp_ajax_register_login_login_form' );
	



