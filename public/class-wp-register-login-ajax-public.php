<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://github.com/royrakesh
 * @since      1.0.0
 *
 * @package    Wp_Register_Login_Ajax
 * @subpackage Wp_Register_Login_Ajax/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Wp_Register_Login_Ajax
 * @subpackage Wp_Register_Login_Ajax/public
 * @author     Rakesh Roy <rakesh@appycodes.com>
 */
class Wp_Register_Login_Ajax_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wp_Register_Login_Ajax_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wp_Register_Login_Ajax_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		
	

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wp-register-login-ajax-public.css', array(), $this->version, 'all' );

		
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wp_Register_Login_Ajax_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wp_Register_Login_Ajax_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */



	wp_register_script( 'wp-register-login-ajax-public-mainjs',
						 plugin_dir_url( __FILE__ ) . 'js/wp-register-login-ajax-public.js', 
						 array(), $this->version, true );
	

	wp_localize_script('wp-register-login-ajax-public-mainjs', 
			            'wrlaobj' , 
			            array(  'wprla_url' => admin_url( 'admin-ajax.php' ) ,
			              		'wprla_nonce' => wp_create_nonce('wprla_ajax_nonce'),
			              		'wprla_redirect_url' => admin_url()
			              	  ));

	wp_enqueue_script( 'wp-register-login-ajax-public-mainjs' );




    


	}


	/**
	 * Public function for  Ajax login from Html form
	 */

	public function wp_ajax_wrlx_login(){

		 if(!empty($_POST['wprla_username'])){
	    	$wprla_username = $_POST['wprla_username'];
		 }
		 if(!empty($_POST['wprla_password'])){
	    	$wprla_password = $_POST['wprla_password'];
		 }

		 $creds = array(
        	'user_login'    => $wprla_username,
        	'user_password' => $wprla_password,
        	'remember'      => true
          );

		 $user = wp_signon( $creds, false );
 
	    if ( is_wp_error( $user ) ) {
	        echo json_encode(array('msg'=> $user->get_error_message(), 'error'=>'true')) ;
	    }else{
	    	echo json_encode(array('msg'=> 'Successfull login', 'error'=>'false')) ;
	    }

		
		
		die();
		

	}



}



	
