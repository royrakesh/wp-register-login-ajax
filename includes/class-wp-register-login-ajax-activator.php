<?php

/**
 * Fired during plugin activation
 *
 * @link       https://github.com/royrakesh
 * @since      1.0.0
 *
 * @package    Wp_Register_Login_Ajax
 * @subpackage Wp_Register_Login_Ajax/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wp_Register_Login_Ajax
 * @subpackage Wp_Register_Login_Ajax/includes
 * @author     Rakesh Roy <rakesh@appycodes.com>
 */
class Wp_Register_Login_Ajax_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
