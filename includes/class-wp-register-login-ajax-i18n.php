<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://github.com/royrakesh
 * @since      1.0.0
 *
 * @package    Wp_Register_Login_Ajax
 * @subpackage Wp_Register_Login_Ajax/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Wp_Register_Login_Ajax
 * @subpackage Wp_Register_Login_Ajax/includes
 * @author     Rakesh Roy <rakesh@appycodes.com>
 */
class Wp_Register_Login_Ajax_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'wp-register-login-ajax',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
