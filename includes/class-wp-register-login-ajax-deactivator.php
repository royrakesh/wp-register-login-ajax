<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://github.com/royrakesh
 * @since      1.0.0
 *
 * @package    Wp_Register_Login_Ajax
 * @subpackage Wp_Register_Login_Ajax/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Wp_Register_Login_Ajax
 * @subpackage Wp_Register_Login_Ajax/includes
 * @author     Rakesh Roy <rakesh@appycodes.com>
 */
class Wp_Register_Login_Ajax_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
